<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <title>Hello, world!</title>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">E-LEARNING</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registro</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar clases</button>
            </form>
        </div>
    </nav>
</header>


<main>

    <div class="container">
        <div class="row row-app">
            <div class="col-md-12">
                <h2>
                    Titulo del video
                </h2>
            </div>

        </div>
        <div class="row">

            <div class="col-md-8">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/I75CUdSJifw" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-md-4">
                <div class="list-group">
                    <button type="button" class="list-group-item list-group-item-action active">
                        Objetivos de la clase
                    </button>
                    <button type="button" class="list-group-item list-group-item-action">Dapibus ac facilisis in
                    </button>
                    <button type="button" class="list-group-item list-group-item-action">Morbi leo risus</button>
                    <button type="button" class="list-group-item list-group-item-action">Porta ac consectetur ac
                    </button>
                    <button type="button" class="list-group-item list-group-item-action" disabled>Vestibulum at eros
                    </button>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="card card-app bg-light col-md-12">
                <div class="card-body">
                    <h5 class="card-title">
                        Descripcion de la clase
                    </h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
    </div>


</main>


<footer>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h5>
                    Sobre la plataforma
                </h5>
                <p>
                    Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto
                    de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución
                    más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí,
                    contenido aquí".
                </p>
            </div>

            <div class="col-md-5">
                <h5>
                    Contactenos
                </h5>
                <p>
                <ul>
                    <li>Tel: 8758769886</li>
                    <li>Dir: Calle 12 # 24</li>
                    <li>Skype: miskype</li>
                    <li>Bogotá - Colombia</li>
                </ul>
                </p>
            </div>
        </div>
    </div>

</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>
</html>