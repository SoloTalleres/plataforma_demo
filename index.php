<?php
$host="localhost";
$port=8889;
$socket="";
$user="root";
$password="root";
$dbname="plataforma";

$con = new mysqli($host, $user, $password, $dbname, $port, $socket)
or die ('Could not connect to the database server' . mysqli_connect_error());

//$con->close();
$query = "SELECT id, titulo, descripcion, video, imagen  FROM plataforma.clases";
?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">E-LEARNING</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registro</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar clases</button>
            </form>
        </div>
    </nav>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4 text-center">Video Clases</h1>
            <p class="lead text-center">
                This is a modified jumbotron that occupies the entire horizontal space of its parent.
                <br>
                that occupies the entire horizontal space of its parent.
            </p>
            <p class="text-center">
                <a href="" class="btn btn-primary "> Soy Docente</a>
                <a href="" class="btn btn-secondary "> Soy Estudiante</a>
            </p>
        </div>
    </div>

</header>


<main>

    <div class="container">
        <div class="row">

            <?php
                if ($stmt = $con->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $titulo , $descripcion , $video, $imagen);
                    while ($stmt->fetch()) {


            ?>

            <div class="col-md-4">
                <div class="card" style="width: 18rem; margin-bottom: 20px">
                    <img class="card-img-top" src="imagenes/<?= $imagen?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">
                            <?= $titulo?>
                        </h5>
                        <p class="card-text">
                            <?= $descripcion?>
                        </p>
                        <a href="clases.php" class="btn btn-primary">
                            Ir al curso
                        </a>
                    </div>
                </div>
            </div>


            <?php
                    }
                    $stmt->close();
                }
            ?>

        </div>
    </div>


</main>


<footer>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h5>
                    Sobre la plataforma
                </h5>
                <p>
                    Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".
                </p>
            </div>

            <div class="col-md-5">
                <h5>
                    Contactenos
                </h5>
                <p>
                    <ul>
                    <li>Tel: 8758769886</li>
                    <li>Dir: Calle 12 # 24</li>
                    <li>Skype: miskype</li>
                    <li>Bogotá - Colombia</li>
                </ul>
                </p>
            </div>
        </div>
    </div>

</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>